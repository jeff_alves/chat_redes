#!/usr/local/bin/python2.7
# -*- coding: utf-8 -*-

'''
Created on 4 de mai de 2016
@author: jeff
'''

from threading import Thread
import signal
import socket
import sys
import util


server = None


class Cliente(Thread):
    def __init__(self, server, con, address, b_size):
        Thread.__init__(self)
        self.setDaemon(True)
        self.server = server
        self.con = con
        self.address = address
        self.nick = None
        self.b_size = b_size
        self.pac_key = util.get_key()
        self.ativo = True
        self.dados = ''

    def run(self):
        self.send([2, "Qual seu nick?"])
        while self.ativo:
            try:
                self.dados += self.con.recv(self.b_size)
                if self.dados:
                    pacotes, resto = util.decode_pac(self.pac_key, self.dados)
                    if len(pacotes):
                        self.dados = resto
                    else:
                        self.dados += resto
                    print('Recebeu', pacotes, 'de',  self.address, self.nick)
                    self.pacotes_handler(pacotes)
                else:
                    print('Cliente encerrou a conexao', self.address, self.nick)  # @IgnorePep8
                    self.stop()
            except Exception as e:
                print('Erro 2', e)
                self.stop()

    def stop(self):
        self.ativo = False
        print('Finalizando conexao', self.address, self.nick)
        self.server.remove(self)
        self.server.send_exit_client(self.nick)
        self.con.close()
        exit(0)

    def send(self, lista):
        print('Enviando', lista, 'para',  self.address, self.nick)
        try:
            pac = util.encode_pac(self.pac_key, lista)
            self.con.sendall(pac)
        except Exception as e:
            print('Erro 3', e, e.message, e.__doc__)

    def pacotes_handler(self, pacotes):
        for pacote in pacotes:
            pacote = pacote[0]
            if not self.nick:
                if self.server.cliente_existe(pacote):
                    self.send([4, 'nick_invalid', 'Nick em uso. Tente outro'])
                else:
                    self.nick = pacote
                    self.server.clientes_nauth.remove(self)
                    self.server.clientes_auth.append(self)
                    self.send([4, 'nick_valid', 'Bem Vindo '+self.nick])
                    self.send([4, 'update_nicks', self.server.get_clientes_str()])  # @IgnorePep8
                    self.send([2, util.get_logo()])
                    self.server.send_new_client(self.nick)
            else:
                if pacote.startswith('/'):
                    pacote = pacote.split(' ', 2)
                    if pacote[0] == '/nick' and pacote[1]:
                        if self.server.cliente_existe(pacote[1]):
                            self.send([4, 'nick_invalid', 'Nick em uso. Tente outro'])  # @IgnorePep8
                        else:
                            self.server.send_update_nick(self.nick, pacote[1])
                            self.nick = pacote[1]
                    elif pacote[0] == '/help':
                        self.send([2, 'Lista de comandos:\n/help\n/nick (novo_nick)'])  # @IgnorePep8
                    else:
                        self.send([2, 'Comando não encontrado. Tente /help'])  # @IgnorePep8
                else:
                    self.server.send_all([1, self.nick, pacote])


class Server():
    def __init__(self, port):
        self.address = ('', port)
        self.backlog = 10
        self.b_size = 2048
        self.server_con = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.inputs = [self.server_con]
        self.outputs = []

        self.clientes_nauth = []
        self.clientes_auth = []

        self.pac_key = util.get_key()
        self.ativo = True

    def start_listening(self):
        try:
            self.server_con.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)  # @IgnorePep8
            self.server_con.bind(self.address)
            self.server_con.listen(self.backlog)
        except Exception as e:
            print(e)
            exit(1)
        print('Servidor iniciado em: %s:%d' % (util.get_ip(), self.address[1]))
        while self.ativo:
            try:
                cli_con, cli_address = self.server_con.accept()
                print('Nova conexao de: %s:%d' % cli_address)
                c = Cliente(self, cli_con, cli_address, self.b_size)
                self.clientes_nauth.append(c)
                c.start()
            except Exception as e:
                print('Erro 1', e)

    def stop_listening(self):
        self.ativo = False
        print('Finalizando Servidor')
        for cliente in self.clientes_auth:
            cliente.stop()
        for cliente in self.clientes_nauth:
            cliente.stop()
        self.server_con.close()
        exit(0)

    def remove(self, cliente):
        if cliente in self.clientes_nauth:
            self.clientes_nauth.remove(cliente)
        if cliente in self.clientes_auth:
            self.clientes_auth.remove(cliente)

    def send_all(self, lista):
        for cliente in self.clientes_auth:
            cliente.send(lista)

    def send_update_nick(self, old, new):
        self.send_all([4, 'update_nick', old+' '+new])
        self.send_all([2, old+' alterou o nick para '+new])

    def send_new_client(self, nick):
        self.send_all([4, 'add_nick', nick])
        self.send_all([2, nick+' entrou na sala'])

    def send_exit_client(self, nick):
        if nick:
            self.send_all([4, 'rem_nick', nick])
            self.send_all([2, nick+' saiu da sala'])

    def cliente_existe(self, nick):
        for cliente in self.clientes_auth:
            if cliente.nick.lower() == nick.lower():
                return True
        return False

    def get_clientes_str(self):
        lista = ''
        for cliente in self.clientes_auth:
            lista += ' '+cliente.nick
        return lista.strip()


def signal_handler(signal, frame):
    global server
    print('Ctrl+C')
    server.stop_listening()
    exit(0)


def main():
    global server
    # util.reopen_with_sudo(2)
    port = 1111
    if len(sys.argv) == 2:
        port = int(sys.argv[1])
    signal.signal(signal.SIGINT, signal_handler)
    server = Server(port)
    server.start_listening()

if __name__ == '__main__':
    exit(main())
