#!/usr/local/bin/python2.7
# -*- coding: utf-8 -*-

'''
Created on 4 de mai de 2016
@author: jeff
'''

from socket import errno
import os
import socket
import sys


def from_Qstr(qstr):
    return unicode(qstr.toUtf8(), encoding="UTF-8").encode('utf-8').strip()


def to_Uni(s):
    return unicode(s.strip(), encoding="UTF-8")


def decode_pac(key, dados):
    pacotes = []
    sep = '-'+str(key)+'-'
    end = '--'+str(key)+'\r\n'
    resto = dados
    while True:
        try:
            index = resto.index(end)
            pacote = resto[:index]
            pacotes.append(pacote.split(sep))
            resto = resto[index+len(end):]
        except Exception:
            break
    return pacotes, resto


# 1 nick msg
# 2 msg
# 3 status coments
# 4 comand args
def encode_pac(key, var_list):
    if len(var_list):
        sep = '-'+str(key)+'-'
        end = '--'+str(key)+'\r\n'
        pac = str(var_list[0])
        for var in var_list[1:]:
            pac += sep+str(var)
        pac += end
        return pac
    return None


def get_ip():
    try:
        return ([l for l in ([ip for ip in socket.gethostbyname_ex(socket.gethostname())[2] if not ip.startswith("127.")][:1], [[(s.connect(('8.8.8.8', 53)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1]]) if l][0][0])# @IgnorePep8
    except socket.error as e:
        if e.errno == errno.ENETUNREACH:
            return '127.0.0.1'
        else:
            raise e
    except Exception as e:
        print('Erro1: ', type(e), e, e.message, e.__doc__)
        return '127.0.0.1'


def is_root():
    return os.geteuid() == 0


def reopen_with_sudo(tipo):
    if not is_root():
        print("Atenção! Precisa ter permissão de root para abrir sockets. Tentando com sudo")# @IgnorePep8
        args = ['sudo', sys.executable] + sys.argv + [os.environ]
        os.execlpe('sudo', *args)


def get_key():
    return 'I5u!3Nm&7s'


def get_logo():
    return (
     '|--------------------------------------------------------------------------------------|\n'# @IgnorePep8
     '|            MMMMMMMMM                                                                 |\n' # @IgnorePep8
     '|          MMMMMMMMMMMMM     .·°¯°·.¸.·°¯°·.¸.·°¯°·.¸.·°¯°·.¸.·°¯°·.¸.·°¯°·.¸.·°¯°·.   |\n' # @IgnorePep8
     '| .   .   MMMMMMMMMMMMMMM    °·.¸.·°¯°·.¸.·°¯°·.¸.·°¯°·.¸.·°¯°·.¸.·°¯°·.¸.·°¯°·.¸.·°   |\n' # @IgnorePep8
     '| M   M   M .MMMMMMMMM. M    .·°¯°·.¸.·°¯°·.¸.·°¯°·.¸.·°¯°·.¸.·°¯°·.¸.·°¯°·.¸.·°¯°·.   |\n' # @IgnorePep8
     '|  M. M   M   .MMMMM.   M    °·.¸.·°¯°·.¸.·°¯°·.¸.·°¯°·.¸.·°¯°·.¸.·°¯°·.¸.·°¯°·.¸.·°   |\n' # @IgnorePep8
     '|   MMMM   MN   MMM   NM     .·°¯°·.¸.·°¯°·.¸.·°¯°·.¸.·°¯°·.¸.·°¯°·.¸.·°¯°·.¸.·°¯°·.   |\n' # @IgnorePep8
     '|   MMMM    MMMMM.MMMMM       ____                    __     ___           _           |\n' # @IgnorePep8
     '|     MM.    MMMMMMMMM       | __ )  ___ _ __ ___     \ \   / (_)_ __   __| | ___      |\n' # @IgnorePep8
     "|      MM     MM-.-MM        |  _ \ / _ \ '_ ` _ \  ___\ \ / /| | '_ \ / _` |/ _ \     |\n" # @IgnorePep8
     '|      .MM.     MMM          | |_) |  __/ | | | | ||___|\ V / | | | | | (_| | (_) |    |\n' # @IgnorePep8
     '|        MMMMMMMMMMMMM.      |____/ \___|_| |_| |_|      \_/  |_|_| |_|\__,_|\___/     |\n' # @IgnorePep8
     '|            MMMMMMMMMMM                                                               |\n' # @IgnorePep8
     '|            MMMMMMMMM MM       _       __  __ _       ____                            |\n' # @IgnorePep8
     '|           .MMMMMMMMM. MM     | | ___ / _|/ _( )___  / ___|  ___ _ ____   _____ _ __  |\n' # @IgnorePep8
     "|           .MMMMMMMMM. MM  _  | |/ _ \ |_| |_|// __| \___ \ / _ \ '__\ \ / / _ \ '__| |\n" # @IgnorePep8
     '|           .MMMMMMMMM. M  | |_| |  __/  _|  _| \__ \  ___) |  __/ |   \ V /  __/ |    |\n' # @IgnorePep8
     '|            .MMMMMMM.  O   \___/ \___|_| |_|   |___/ |____/ \___|_|    \_/ \___|_|    |\n' # @IgnorePep8
     '|             MMMMMMM.                                                                 |\n' # @IgnorePep8
     '|             MMM MMM        °·.¸.·°¯°·.¸.·°¯°·.¸.·°¯°·.¸.·°¯°·.¸.·°¯°·.¸.·°¯°·.¸.·°   |\n' # @IgnorePep8
     '|             MMM MMM        .·°¯°·.¸.·°¯°·.¸.·°¯°·.¸.·°¯°·.¸.·°¯°·.¸.·°¯°·.¸.·°¯°·.   |\n' # @IgnorePep8
     '|             MMM MMM        °·.¸.·°¯°·.¸.·°¯°·.¸.·°¯°·.¸.·°¯°·.¸.·°¯°·.¸.·°¯°·.¸.·°   |\n' # @IgnorePep8
     '|             MMM MMM        .·°¯°·.¸.·°¯°·.¸.·°¯°·.¸.·°¯°·.¸.·°¯°·.¸.·°¯°·.¸.·°¯°·.   |\n' # @IgnorePep8
     '|          MMMMM   MMMMM.    °·.¸.·°¯°·.¸.·°¯°·.¸.·°¯°·.¸.·°¯°·.¸.·°¯°·.¸.·°¯°·.¸.·°   |\n' # @IgnorePep8
     '|         MMMMMM   MMMMMM                                                              |\n' # @IgnorePep8
     '|--------------------------------------------------------------------------------------|\n'# @IgnorePep8
     )
