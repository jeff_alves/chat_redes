#!/usr/local/bin/python2.7
# -*- coding: utf-8 -*-

'''
Created on 18 de mai de 2016
@author: jeff
'''

import Queue
import select
import signal
import socket
import sys

import util

server = None


class Cliente():
    def __init__(self, address):
        self.address = address
        self.nick = None
        self.out_queue = Queue.Queue()
        self.dados = ''


class Server():
    def __init__(self, port):
        self.address = ('', port)
        self.backlog = 10
        self.b_size = 2048
        self.server_con = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.inputs = [self.server_con]
        self.outputs = []

        self.clientes_nauth = {}
        self.clientes_auth = {}

        self.pac_key = util.get_key()
        self.ativo = True

    def start_listening(self):
        try:
            self.server_con.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)  # @IgnorePep8
            self.server_con.bind(self.address)
            self.server_con.listen(self.backlog)
            self.server_con.setblocking(0)
        except Exception as e:
            print(e)
            exit(1)
        print('Servidor iniciado em: %s:%d' % (util.get_ip(), self.address[1]))
        while self.ativo:
            r, w, e = select.select(self.inputs, self.outputs, self.inputs, .5)
            for con in r:
                if con == self.server_con:  # servidor
                    cli_con, cli_address = con.accept()
                    print('Nova conexao de: %s:%d' % cli_address)
                    cli_con.setblocking(0)
                    self.clientes_nauth[cli_con] = Cliente(cli_address)
                    self.inputs.append(cli_con)
                    self.send(self.clientes_nauth[cli_con], cli_con, [2, "Qual seu nick?"])# @IgnorePep8
                else:  # clientes
                    cli = self.get_cli(con)
                    cli.dados += con.recv(self.b_size)
                    if cli.dados:
                        pacotes, resto = util.decode_pac(self.pac_key, cli.dados)  # @IgnorePep8
                        if len(pacotes):
                            cli.dados = resto
                        else:
                            cli.dados += resto
                        print('Recebeu', pacotes, 'de',  cli.address, cli.nick)
                        self.pacotes_handler(cli, con, pacotes)
                    else:
                        print('Cliente encerrou a conexao', cli.address, cli.nick)  # @IgnorePep8
                        self.finaliza_con(con)

            for con in w:
                cli = self.get_cli(con)
                while not cli.out_queue.empty():
                    dados = cli.out_queue.get_nowait()
                    pac = util.encode_pac(self.pac_key, dados)
                    print('Enviando', dados, 'para',  cli.address, cli.nick)
                    con.send(pac)
                self.outputs.remove(con)

            for con in e:
                print('Erro na conexao: ', con.getpeername())
                if con != self.server_con:
                    self.finaliza_con(con)

    def stop_listening(self):
        self.ativo = False
        print('Finalizando Servidor')
        for con in self.clientes_auth.keys():
            self.finaliza_con(con)
        for con in self.clientes_nauth.keys():
            self.finaliza_con(con)
        self.server_con.close()
        exit(0)

    def finaliza_con(self, con):
        cli = self.get_cli(con)
        print('Finalizando conexao', cli.address, cli.nick)
        self.inputs.remove(con)
        if con in self.outputs:
            self.outputs.remove(con)
        if con in self.clientes_nauth:
            del self.clientes_nauth[con]
        if con in self.clientes_auth:
            del self.clientes_auth[con]
            self.send_exit_client(cli.nick)
        con.close()

    def get_cli(self, con):
        if con in self.clientes_nauth:
            return self.clientes_nauth[con]
        return self.clientes_auth[con]

    def send(self, cli, con, lista):
        cli.out_queue.put(lista)
        if con not in self.outputs:
            self.outputs.append(con)

    def send_all(self, lista):
        for con in self.clientes_auth:
            cli = self.clientes_auth[con]
            self.send(cli, con, lista)

    def pacotes_handler(self, cli, con, pacotes):
        for pacote in pacotes:
            pacote = pacote[0]
            if con in self.clientes_nauth:
                if self.cliente_existe(pacote):
                    self.send(cli, con, [4, 'nick_invalid', 'Nick em uso. Tente outro'])  # @IgnorePep8
                else:
                    cli.nick = pacote
                    self.clientes_auth[con] = self.clientes_nauth.pop(con)
                    self.send(cli, con, [4, 'nick_valid', 'Bem Vindo '+cli.nick])  # @IgnorePep8
                    self.send(cli, con, [4, 'update_nicks', self.get_clientes_str()])  # @IgnorePep8
                    self.send(cli, con, [2, util.get_logo()])
                    self.send_new_client(cli.nick)
            else:
                if pacote.startswith('/'):
                    pacote = pacote.split(' ', 2)
                    if pacote[0] == '/nick' and pacote[1]:
                        if self.cliente_existe(pacote[1]):
                            self.send(cli, con, [4, 'nick_invalid', 'Nick em uso. Tente outro'])  # @IgnorePep8
                        else:
                            self.send_update_nick(cli.nick, pacote[1])
                            cli.nick = pacote[1]
                    elif pacote[0] == '/help':
                        self.send(cli, con, [2, 'Lista de comandos:\n/help\n/nick (novo_nick)'])  # @IgnorePep8
                    else:
                        self.send(cli, con, [2, 'Comando não encontrado. Tente /help'])  # @IgnorePep8
                else:
                    self.send_all([1, cli.nick, pacote])

    def send_update_nick(self, old, new):
        self.send_all([4, 'update_nick', old+' '+new])
        self.send_all([2, old+' alterou o nick para '+new])

    def send_new_client(self, nick):
        self.send_all([4, 'add_nick', nick])
        self.send_all([2, nick+' entrou na sala'])

    def send_exit_client(self, nick):
        if nick:
            self.send_all([4, 'rem_nick', nick])
            self.send_all([2, nick+' saiu da sala'])

    def cliente_existe(self, nick):
        for con in self.clientes_auth:
            cli = self.clientes_auth[con]
            if cli.nick.lower() == nick.lower():
                return True
        return False

    def get_clientes_str(self):
        lista = ''
        for con in self.clientes_auth:
            cli = self.clientes_auth[con]
            lista += ' '+cli.nick
        return lista.strip()


def signal_handler(signal, frame):
    global server
    print('Ctrl+C')
    server.stop_listening()
    exit(0)


def main():
    global server
    # util.reopen_with_sudo(2)
    port = 1111
    if len(sys.argv) == 2:
        port = int(sys.argv[1])
    signal.signal(signal.SIGINT, signal_handler)
    server = Server(port)
    server.start_listening()

if __name__ == '__main__':
    exit(main())
