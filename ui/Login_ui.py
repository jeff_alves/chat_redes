# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Login.ui'
#
# Created: Thu May 26 14:18:22 2016
#      by: PyQt4 UI code generator 4.11.2
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Login(object):
    def setupUi(self, Login):
        Login.setObjectName(_fromUtf8("Login"))
        self.centralwidget = QtGui.QWidget(Login)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.gridLayout = QtGui.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        spacerItem = QtGui.QSpacerItem(20, 0, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.gridLayout.addItem(spacerItem, 0, 1, 1, 1)
        spacerItem1 = QtGui.QSpacerItem(20, 0, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.gridLayout.addItem(spacerItem1, 2, 1, 1, 1)
        spacerItem2 = QtGui.QSpacerItem(2, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem2, 1, 2, 1, 1)
        spacerItem3 = QtGui.QSpacerItem(2, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem3, 1, 0, 1, 1)
        self.frame = QtGui.QFrame(self.centralwidget)
        self.frame.setMinimumSize(QtCore.QSize(0, 0))
        self.frame.setFrameShape(QtGui.QFrame.NoFrame)
        self.frame.setFrameShadow(QtGui.QFrame.Raised)
        self.frame.setObjectName(_fromUtf8("frame"))
        self.gridLayout_2 = QtGui.QGridLayout(self.frame)
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        spacerItem4 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.gridLayout_2.addItem(spacerItem4, 3, 4, 1, 1)
        self.lvlNick = QtGui.QLabel(self.frame)
        self.lvlNick.setObjectName(_fromUtf8("lvlNick"))
        self.gridLayout_2.addWidget(self.lvlNick, 2, 0, 1, 1)
        self.txtNick = QtGui.QLineEdit(self.frame)
        self.txtNick.setObjectName(_fromUtf8("txtNick"))
        self.gridLayout_2.addWidget(self.txtNick, 2, 1, 1, 4)
        self.lblServidor = QtGui.QLabel(self.frame)
        self.lblServidor.setObjectName(_fromUtf8("lblServidor"))
        self.gridLayout_2.addWidget(self.lblServidor, 1, 0, 1, 1)
        spacerItem5 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.gridLayout_2.addItem(spacerItem5, 3, 3, 1, 1)
        self.txtServidor = QtGui.QLineEdit(self.frame)
        self.txtServidor.setObjectName(_fromUtf8("txtServidor"))
        self.gridLayout_2.addWidget(self.txtServidor, 1, 1, 1, 4)
        self.btnEntrar = QtGui.QPushButton(self.frame)
        self.btnEntrar.setObjectName(_fromUtf8("btnEntrar"))
        self.gridLayout_2.addWidget(self.btnEntrar, 3, 2, 1, 1)
        spacerItem6 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.gridLayout_2.addItem(spacerItem6, 3, 0, 1, 1)
        spacerItem7 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.gridLayout_2.addItem(spacerItem7, 3, 1, 1, 1)
        self.gridLayout.addWidget(self.frame, 1, 1, 1, 1)
        Login.setCentralWidget(self.centralwidget)
        self.lvlNick.setBuddy(self.txtNick)
        self.lblServidor.setBuddy(self.txtServidor)

        self.retranslateUi(Login)
        QtCore.QObject.connect(self.txtServidor, QtCore.SIGNAL(_fromUtf8("returnPressed()")), self.txtNick.setFocus)
        QtCore.QObject.connect(self.btnEntrar, QtCore.SIGNAL(_fromUtf8("clicked()")), Login.act_entrar)
        QtCore.QObject.connect(self.txtNick, QtCore.SIGNAL(_fromUtf8("returnPressed()")), Login.act_entrar)
        QtCore.QMetaObject.connectSlotsByName(Login)
        Login.setTabOrder(self.txtServidor, self.txtNick)
        Login.setTabOrder(self.txtNick, self.btnEntrar)

    def retranslateUi(self, Login):
        Login.setWindowTitle(_translate("Login", "Login", None))
        self.lvlNick.setText(_translate("Login", "Nick:", None))
        self.txtNick.setPlaceholderText(_translate("Login", "Seu_Nick", None))
        self.lblServidor.setText(_translate("Login", "Servidor:", None))
        self.txtServidor.setPlaceholderText(_translate("Login", "IP[:porta]", None))
        self.btnEntrar.setText(_translate("Login", "Entrar", None))

