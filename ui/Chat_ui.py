# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Chat.ui'
#
# Created: Thu May 26 14:18:22 2016
#      by: PyQt4 UI code generator 4.11.2
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Chat(object):
    def setupUi(self, Chat):
        Chat.setObjectName(_fromUtf8("Chat"))
        Chat.resize(780, 450)
        self.centralwidget = QtGui.QWidget(Chat)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.gridLayout = QtGui.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.txtMsg = QtGui.QLineEdit(self.centralwidget)
        self.txtMsg.setInputMask(_fromUtf8(""))
        self.txtMsg.setMaxLength(1024)
        self.txtMsg.setObjectName(_fromUtf8("txtMsg"))
        self.gridLayout.addWidget(self.txtMsg, 1, 0, 1, 1)
        self.lboxUsuarios = QtGui.QListView(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lboxUsuarios.sizePolicy().hasHeightForWidth())
        self.lboxUsuarios.setSizePolicy(sizePolicy)
        self.lboxUsuarios.setSelectionMode(QtGui.QAbstractItemView.NoSelection)
        self.lboxUsuarios.setObjectName(_fromUtf8("lboxUsuarios"))
        self.gridLayout.addWidget(self.lboxUsuarios, 0, 1, 2, 1)
        self.txtChat = QtGui.QTextEdit(self.centralwidget)
        self.txtChat.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.txtChat.setTabChangesFocus(True)
        self.txtChat.setDocumentTitle(_fromUtf8(""))
        self.txtChat.setUndoRedoEnabled(False)
        self.txtChat.setReadOnly(True)
        self.txtChat.setHtml(_fromUtf8("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'Noto Sans\'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body></html>"))
        self.txtChat.setTextInteractionFlags(QtCore.Qt.LinksAccessibleByMouse|QtCore.Qt.TextSelectableByMouse)
        self.txtChat.setObjectName(_fromUtf8("txtChat"))
        self.gridLayout.addWidget(self.txtChat, 0, 0, 1, 1)
        self.gridLayout.setColumnStretch(0, 85)
        self.gridLayout.setColumnStretch(1, 15)
        Chat.setCentralWidget(self.centralwidget)

        self.retranslateUi(Chat)
        QtCore.QObject.connect(self.txtMsg, QtCore.SIGNAL(_fromUtf8("returnPressed()")), Chat.act_enviar)
        QtCore.QMetaObject.connectSlotsByName(Chat)
        Chat.setTabOrder(self.txtMsg, self.txtChat)
        Chat.setTabOrder(self.txtChat, self.lboxUsuarios)

    def retranslateUi(self, Chat):
        Chat.setWindowTitle(_translate("Chat", "Chat", None))
        self.txtMsg.setPlaceholderText(_translate("Chat", "Envie sua mensagem...", None))

