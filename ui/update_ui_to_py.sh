#!/bin/bash
for filename in *.ui; do
	name=$(echo "$filename" | sed -r 's/\./_/g')
	pyuic4 "$filename" > "$name".py
done
