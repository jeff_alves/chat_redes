Trabalho de Redes
----------------------
Aplicativo de chat cliente e servidor com socket bloqueante e não bloqueante.
Aluno: Jefferson Alves
https://bitbucket.org/jeff_alves/chat_redes



Requisitos
----------------------
Python 2.7		-> sudo apt-get install python2.7
PyQt4			-> sudo apt-get install python-qt4
Testado em Linux e Windows



Como usar (Opções entre colchetes são opcionais)
----------------------
Ligar o servidor (porta padrão: 1111):
	Bloqueante:		python server_block.py [porta]
	Não bloqueante:	python server_non_block.py [porta]

Abrir cliente:
	Bloqueante:		python client_block.py
	Não bloqueante:	python client_non_block.py
	Depois de aberto, colocar IP[:porta] do servidor, e um nick para ser usado.