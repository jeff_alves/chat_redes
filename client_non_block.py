#!/usr/local/bin/python2.7
# -*- coding: utf-8 -*-

'''
Created on 18 de mai de 2016
@author: jeff
'''

from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import pyqtSignal
from PyQt4.QtGui import QMessageBox, QStandardItemModel, QStandardItem, QColor
from thread import start_new_thread
import Queue
import select
import socket
import sys

from ui import Chat_ui, Login_ui
from util import get_ip, from_Qstr, to_Uni, get_key, decode_pac, encode_pac


class Cliente(QtGui.QMainWindow):
    pacotes_handler = pyqtSignal(list)

    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.ui = Login_ui.Ui_Login()
        self.ui.setupUi(self)
        self.ui.txtServidor.setText(get_ip())
        self.pacotes_handler.connect(self.act_pacotes_handler)

        self.list_clients = []
        self.last_chat_nick = None
        self.client_con = None
        self.b_size = 4096

        self.inputs = []
        self.outputs = []
        self.out_queue = Queue.Queue()
        self.dados = ''
        self.nick = None

        self.pac_key = get_key()
        self.ativo = True

    def show_chat(self):
        self.ui.centralwidget.close()
        del self.ui
        self.ui = Chat_ui.Ui_Chat()
        self.ui.setupUi(self)
        self.setWindowTitle(to_Uni('Chat ' + str(self.client_con.getpeername())))#@IgnorePep8
        self.ui.txtMsg.setFocus()

    def act_enviar(self):
        if len(from_Qstr(self.ui.txtMsg.text())):
            msg = from_Qstr(self.ui.txtMsg.text())
            self.ui.txtMsg.setText('')
            self.send([msg])

    def act_entrar(self):
        self.nick = from_Qstr(self.ui.txtNick.text()).replace(' ', '_')
        if len(self.nick) < 3 or len(self.nick) > 15:
            QMessageBox.about(self, to_Uni('Atenção'), to_Uni('Nick deve ter entre três e quinze caracteres.'))#@IgnorePep8
            return
        server = from_Qstr(self.ui.txtServidor.text()).split(':')
        host = server[0]
        porta = 1111
        if len(server) > 1:
            porta = int(server[1])
        try:
            self.ativo = True
            self.client_con = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.client_con.connect((host, porta))
            start_new_thread(self.start_listening, (None,))
            self.send([self.nick])
        except Exception as e:
            QMessageBox.about(self, to_Uni('Atenção'), to_Uni(str(e) + '\nVerifique o endereço de IP e porta.'))#@IgnorePep8
            return

    def start_listening(self, a):
        self.client_con.setblocking(0)
        self.inputs.append(self.client_con)
        while self.ativo:
            try:
                r, w, e = select.select(self.inputs, self.outputs, self.inputs, .3)#@IgnorePep8
                for con in r:
                    self.dados += con.recv(self.b_size)
                    if self.dados:
                        pacotes, resto = decode_pac(self.pac_key, self.dados)
                        if len(pacotes):
                            self.dados = resto
                        else:
                            self.dados += resto
                        self.pacotes_handler.emit(pacotes)
                    else:
                        self.escrever_chat('Servidor encerrou a conexao')# @IgnorePep8
                        self.stop_listening()

                for con in w:
                    while not self.out_queue.empty():
                        dados = self.out_queue.get_nowait()
                        pac = encode_pac(self.pac_key, dados)
                        print('Enviando', dados, 'para', con.getpeername())
                        con.send(pac)
                    self.outputs.remove(con)

                for con in e:
                    print('Erro na conexao: ', con.getpeername())
                    if con != self.client_con:
                        self.finaliza_con(con)
            except Exception as e:
                print('Erro 2', e)

    def stop_listening(self):
        self.ativo = False
        print('Finalizando conexao')
        self.inputs.remove(self.client_con)
        if self.client_con in self.outputs:
            self.outputs.remove(self.client_con)
        self.client_con.close()
        exit(0)

    def restart(self):
        self.ativo = False
        if self.client_con in self.inputs:
            self.inputs.remove(self.client_con)
        if self.client_con in self.outputs:
            self.outputs.remove(self.client_con)
        self.out_queue = Queue.Queue()
        self.client_con.close()
        self.dados = ''
        self.nick = None

    def send(self, lista):
        self.out_queue.put(lista)
        if self.client_con not in self.outputs:
            self.outputs.append(self.client_con)

    def act_pacotes_handler(self, pacotes):
        for pacote in pacotes:
            print(pacote)
            tipo = pacote[0]
            arg1 = None
            try:
                arg1 = pacote[1]
            except Exception:
                    pass
            arg2 = None
            try:
                arg2 = pacote[2]
            except Exception:
                    pass
            if tipo == '1':
                self.escrever_chat(arg2, arg1)
            elif tipo == '2':
                self.escrever_chat(arg1)
            elif tipo == '3':
                QMessageBox.about(self, to_Uni('Atenção'), to_Uni(arg2))
            elif tipo == '4':
                if arg1 == 'update_nicks':
                    self.list_clients = arg2.split(' ')
                    self.update_listUsers()
                elif arg1 == 'update_nick':
                    old, new = arg2.split(' ')
                    if old == self.nick:
                        self.nick = new
                    self.list_clients.remove(old)
                    self.list_clients.append(new)
                    self.update_listUsers()
                elif arg1 == 'add_nick':
                    self.list_clients.append(arg2)
                    self.list_clients = list(set(self.list_clients))
                    self.update_listUsers()
                elif arg1 == 'rem_nick':
                    self.list_clients.remove(arg2)
                    self.update_listUsers()
                elif arg1 == 'nick_valid':
                    self.show_chat()
                elif arg1 == 'nick_invalid':
                    if hasattr(self.ui, 'txtChat'):
                        self.escrever_chat(arg2)
                    else:
                        QMessageBox.about(self, to_Uni('Atenção'), to_Uni(arg2))#@IgnorePep8
                        self.restart()

    def escrever_chat(self, msg, nick='Server'):
        if hasattr(self.ui, 'txtChat'):
            diferente = self.last_chat_nick != nick
            minha = nick == self.nick
            if diferente:
                self.ui.txtChat.append(to_Uni(''))
                if nick == 'Server':
                    self.ui.txtChat.setFontFamily("DejaVu Sans Mono")
                    self.ui.txtChat.setAlignment(QtCore.Qt.AlignHCenter)
                    self.ui.txtChat.setTextColor(QColor(0, 0, 255))
                    self.ui.txtChat.setFontPointSize(8)
                    self.ui.txtChat.setFontWeight(100)
                elif not minha:
                    self.ui.txtChat.setFontFamily("Arial")
                    self.ui.txtChat.setAlignment(QtCore.Qt.AlignLeft)
                    self.ui.txtChat.setTextColor(QColor(0, 0, 60))
                    self.ui.txtChat.setFontPointSize(10)
                    self.ui.txtChat.setFontWeight(100)
                    if self.last_chat_nick != nick:
                        self.ui.txtChat.append(to_Uni(nick + ':'))
                elif minha:
                    self.ui.txtChat.setFontFamily("Arial")
                    self.ui.txtChat.setAlignment(QtCore.Qt.AlignRight)
                    self.ui.txtChat.setTextColor(QColor(0, 160, 0))
                    self.ui.txtChat.setFontPointSize(10)
                    self.ui.txtChat.setFontWeight(100)
                    if self.last_chat_nick != nick:
                        self.ui.txtChat.append(to_Uni(nick + ':'))
            for m in msg.split('\n'):
                    self.ui.txtChat.append(to_Uni(m))
            self.last_chat_nick = nick
            self.ui.txtChat.moveCursor(QtGui.QTextCursor.End)
            self.ui.txtChat.ensureCursorVisible()

    def update_listUsers(self):
        model = QStandardItemModel(self.ui.lboxUsuarios)
        for cliente in self.list_clients:
            item = QStandardItem(to_Uni(cliente))
            item.setEditable(False)
            model.appendRow(item)
        self.ui.lboxUsuarios.setModel(model)


if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    c = Cliente()
    c.show()
    sys.exit(app.exec_())
